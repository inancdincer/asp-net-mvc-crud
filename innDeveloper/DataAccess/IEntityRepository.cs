﻿using innDeveloper.CORE.Entitiess;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace innDeveloper.CORE.DataAccess
{
    public interface IEntityRepository<T> where T: class, IEntity,new () 
    {
        //Tek bir nesne çekmek adına bunu kullanırıız.
        //id geçebiliriz, string tc geçebiliriz, uniq olabilir.Bu yüzden linq expression geçiyoruz.
        T Get(Expression<Func<T, bool>> filter = null);
        List<T> GetList(Expression<Func<T, bool>> filter = null);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);


    }
}
