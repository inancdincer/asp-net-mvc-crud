﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using innDeveloper.Business.Abstract;
using innDeveloper.Northwind.Entities.Concrete;
using innDeveloper.Northwind.MvcWebUI.Models;
using innDeveloper.Northwind.MvcWebUI.Services;
using Microsoft.AspNetCore.Mvc;

namespace innDeveloper.Northwind.MvcWebUI.Controllers
{
    public class CartController : Controller
    {
        private ICartSessionServices _cartSessionServices;
        private ICartService _cartService;
        private IProductService _productService;

        public CartController(ICartSessionServices cartSessionServices, ICartService cartService, IProductService productService)
        {
            _cartSessionServices = cartSessionServices;
            _cartService = cartService;
            _productService = productService;
        }
        public ActionResult AddToCart(int productId)
        {
            var productToBeAdded = _productService.GetById(productId);
            var cart = _cartSessionServices.GetCart();
            _cartService.AddToCart(cart,productToBeAdded);
           
            _cartSessionServices.SetCart(cart);
            TempData.Add("message",
                String.Format("Your product, {0} , was succesfully added to the cart!", productToBeAdded.ProductName));
            return RedirectToAction("Index", "Product");
        }

        public ActionResult List()
        {
            var cart = _cartSessionServices.GetCart();
            CartSummaryViewModel model = new CartSummaryViewModel
            {
                Cart = cart
            };
            return View(model);
        }

        public ActionResult Remove(int productId)
        {
            var cart = _cartSessionServices.GetCart();
            _cartService.RemoveFromCart(cart,productId);
            _cartSessionServices.SetCart(cart);
            TempData.Add("message", String.Format("Your product was succesfully removed from the cart"));
            return RedirectToAction("List");
        }

        public ActionResult Complete()
        {
            var ShippingDetailsViewModel = new ShippingDetailsViewModel
            {
                ShippingDetails = new ShippingDetails()
            };
            return View(ShippingDetailsViewModel);
        }
        [HttpPost]
        public ActionResult Complete(ShippingDetails shippingDetails)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            TempData.Add("message", String.Format("Thank you {0}, you order is process", shippingDetails.FirstName));
            return View();
        }
    }
}

