﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using innDeveloper.Northwind.MvcWebUI.Models;
using innDeveloper.Northwind.MvcWebUI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace innDeveloper.Northwind.MvcWebUI.ViewComponents
{
    public class CartSummaryViewComponent :ViewComponent
    {
        private ICartSessionServices _iCartSessionServices;

        public CartSummaryViewComponent(ICartSessionServices iCartSessionServices)
        {
            _iCartSessionServices = iCartSessionServices;
        }

        public ViewViewComponentResult Invoke()
        {
            var model = new CartSummaryViewModel
            {
                Cart = _iCartSessionServices.GetCart()
            };

            return View(model);
        }
    }

  
}
