﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using innDeveloper.Northwind.Entities.Concrete;

namespace innDeveloper.Northwind.MvcWebUI.Services
{
    public interface ICartSessionServices
    {
        Cart GetCart();
        void SetCart(Cart cart);
    }
}
