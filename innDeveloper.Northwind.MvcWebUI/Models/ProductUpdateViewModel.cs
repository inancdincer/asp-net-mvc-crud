﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using innDeveloper.Northwind.Entities.Concrete;

namespace innDeveloper.Northwind.MvcWebUI.Models
{
    public class ProductUpdateViewModel
    {
        public Product Product { get; set; }
        public List<Category> Categories { get; set; }
    }
}
