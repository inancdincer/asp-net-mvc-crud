﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using innDeveloper.Northwind.Entities.Concrete;

namespace innDeveloper.Northwind.MvcWebUI.Models
{
    public class ShippingDetailsViewModel
    {
        public ShippingDetails ShippingDetails { get; set; }
    }
}
