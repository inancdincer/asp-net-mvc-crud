﻿using System;
using System.Collections.Generic;
using System.Text;
using innDeveloper.Northwind.Entities.Concrete;

namespace innDeveloper.Business.Abstract
{
    public interface ICartService
    {
        void AddToCart(Cart cart, Product product);

        void RemoveFromCart(Cart cart, int productId);
        List<CartLine> List(Cart cart);
    }
}
