﻿using innDeveloper.Northwind.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace innDeveloper.Business.Abstract
{
    public interface IProductService
    {
        //IEntity Repositor kullanmamamızın sebebi bütün operasyonların hizmetini vermeyeceğiz.
        // Daha önemlisi veri erişim katmanında sadece nesneyle ilgili işlemler yapılır.
        //Ama burası iş katmanı olduğu için , buraya bir kural göndermek isteyebiliriz.
        //Örneğin kategory id sine gore ürünleri çekme repository de yoktur.

        List<Product> GetAll();
        List<Product> GetByCategory(int categoryId);
        void Add(Product product);
        void Delete(int productId);

        void Update(Product product);
        Product GetById(int  productId);
    }
}
