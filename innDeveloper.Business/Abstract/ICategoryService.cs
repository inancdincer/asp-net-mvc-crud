﻿using innDeveloper.Northwind.Entities.Concrete;
using System.Collections.Generic;

namespace innDeveloper.Business.Abstract
{
    public interface ICategoryService
    {
        List<Category> GetAll();

    }
}
