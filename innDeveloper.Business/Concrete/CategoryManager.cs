﻿using innDeveloper.Business.Abstract;
using innDeveloper.DataAccess.Abstract;
using innDeveloper.Northwind.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace innDeveloper.Business.Concrete
{
    public class CategoryManager : ICategoryService
    {
        private readonly ICategoryDal _categoryDal;
        public CategoryManager(ICategoryDal categoryDal)
        {
            _categoryDal = categoryDal;
        }

        public List<Category> GetAll()
        {
           return  _categoryDal.GetList();
        }
    }
}
