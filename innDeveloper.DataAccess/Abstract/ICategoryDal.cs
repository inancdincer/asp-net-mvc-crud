﻿using innDeveloper.CORE.DataAccess;
using innDeveloper.Northwind.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace innDeveloper.DataAccess.Abstract
{
    public interface ICategoryDal : IEntityRepository<Category>
    {
        //Custom operations 
    }
}
