﻿using innDeveloper.CORE.DataAccess.EntityFramework;
using innDeveloper.DataAccess.Abstract;
using innDeveloper.Northwind.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
namespace innDeveloper.DataAccess.Concrete.EntityFramework
{
    public class EfProductDal : EfEntityRepositoryBase<Product, NorthwindContext>, IProductDal
    {

    }
}
