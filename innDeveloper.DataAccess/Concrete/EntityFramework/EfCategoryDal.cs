﻿using innDeveloper.CORE.DataAccess.EntityFramework;
using innDeveloper.DataAccess.Abstract;
using innDeveloper.Northwind.Entities.Concrete;
namespace innDeveloper.DataAccess.Concrete.EntityFramework
{
    public class EfCategoryDal : EfEntityRepositoryBase<Category,NorthwindContext>, ICategoryDal
    {

    }
}
